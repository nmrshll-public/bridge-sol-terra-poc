// export const ETH_NODE_URL = ci ? "ws://eth-devnet:8545" : "ws://localhost:8545";
// export const ETH_PRIVATE_KEY =
//   "0x4f3edf983ac636a65a842ce7c78d9aa706d3b113bce9c46f30d7d21715b23b1d";
// export const ETH_CORE_BRIDGE_ADDRESS =
//   "0xC89Ce4735882C9F0f0FE26686c53074E09B0D550";
// export const ETH_TOKEN_BRIDGE_ADDRESS =
//   "0x0290FB167208Af455bB137780163b7B7a9a10C16";



// https://docs.wormholenetwork.com/wormhole/contracts#testnet
export const SOLANA_HOST = "https://api.devnet.solana.com";
// ci
//     ? "http://solana-devnet:8899"
//     : "http://localhost:8899";
export const SOLANA_DEVNET_CHAIN_ID = 1;
export const SOLANA_DEVNET_CORE_BRIDGE_ID = "3u8hJUVTA4jH1wYAyUur7FFZVQ8H635K3tSHHF4ssjQ5";
// export const SOLANA_CORE_BRIDGE_ADDRESS =
//     "Bridge1p5gheXUvJ6jGWGeCsgPKgnE3YgdGKRVCMY9o";
export const SOLANA_DEVNET_TOKEN_BRIDGE_ID = "DZnkkTmCiFWfYTfT41X3Rd1kDgozqzxWaHqsw6W4x2oe";
// export const SOLANA_TOKEN_BRIDGE_ADDRESS =
//     "B6RHG3mfcckmrYN1UhmJzyS1XX3fZKbkeUcpJe9Sy3FE";
// export const SOLANA_PRIVATE_KEY = new Uint8Array([
//     14, 173, 153, 4, 176, 224, 201, 111, 32, 237, 183, 185, 159, 247, 22, 161, 89,
//     84, 215, 209, 212, 137, 10, 92, 157, 49, 29, 192, 101, 164, 152, 70, 87, 65,
//     8, 174, 214, 157, 175, 126, 98, 90, 54, 24, 100, 177, 247, 77, 19, 112, 47,
//     44, 165, 109, 233, 102, 14, 86, 109, 29, 134, 145, 132, 141,
// ]);

// Wormhole UST, on solana chain. Address of token mint
// source: https://github.com/certusone/wormhole-token-list/blob/main/content/by_source.csv
export const UST_ON_SOLANA_ID = "9vMJfxuKxXBoEa7rM12mYLMwTacLMLDJqHozw96WQL8i";
// export const TEST_SOLANA_TOKEN = "2WDq7wSs9zYrpx2kbHDA4RUTRch2CCTP6ZWaH4GNfnQQ"; // TODO



export const TERRA_TESTNET_CHAIN_ID = 3;
// export const TERRA_CHAIN_ID = "localterra";
// export const TERRA_TESTNET_LCD_URL = "https://bombay.stakesystems.io";
export const TERRA_TESTNET_LCD_URL = "https://bombay-lcd.terra.dev";
export const TERRA_TESTNET_RPC_URL = "https://bombay.stakesystems.io:2053";
// export const TERRA_NODE_URL = ci
//     ? "http://terra-terrad:1317"
//     : "http://localhost:1317";
// export const TERRA_GAS_PRICES_URL = ci
//     ? "http://terra-fcd:3060/v1/txs/gas_prices"
//     : "http://localhost:3060/v1/txs/gas_prices";
export const TERRA_TESTNET_CORE_BRIDGE_ID = "terra1pd65m0q9tl3v8znnz5f5ltsfegyzah7g42cx5v";
// export const TERRA_CORE_BRIDGE_ADDRESS =
//     "terra18vd8fpwxzck93qlwghaj6arh4p7c5n896xzem5";
export const TERRA_TESTNET_TOKEN_BRIDGE_ID = "terra1pseddrv0yfsn76u4zxrjmtf45kdlmalswdv39a";
// export const TERRA_TOKEN_BRIDGE_ADDRESS =
//     "terra10pyejy66429refv3g35g2t7am0was7ya7kz2a4";
export const TERRA_SEED_PHRASE =
    // "notice oak worry limit wrap speak medal online prefer cluster roof addict wrist behave treat actual wasp year salad speed social layer crew genius";
    "nut icon purpose uniform abuse wedding any anxiety describe talent settle project vacuum later girl deputy target salon trouble picture clutch yellow cherry sphere";
export const TERRA_CHAIN_IDS = {
    bombay: "bombay-12"
}
export const TERRA_TESTNET_GAS_PRICES_URL = 'https://bombay-fcd.terra.dev/v1/txs/gas_prices';

// export const TEST_ERC20 = "0x2D8BE6BF0baA74e0A907016679CaE9190e80dD0A";

export const WORMHOLE_TESTNET_RPC_URL = "https://wormhole-v2-testnet-api.certus.one";
// export const WORMHOLE_RPC_HOSTS = ci
//     ? ["http://guardian:7071"]
//     : ["http://localhost:7071"];
export const WORMHOLE_CHAIN_IDS = {
    SOLANA_TESTNET: 1,
    TERRA_TESTNET: 3,
}


export const TO_9_DECIMALS = 1_000_000_000;