import { promises as fs } from "fs";
import axios from "axios";
import fetch from 'isomorphic-fetch';
import { Connection, PublicKey, Keypair, TransactionResponse } from '@solana/web3.js';
import { NodeHttpTransport } from "@improbable-eng/grpc-web-node-http-transport";
import { ASSOCIATED_TOKEN_PROGRAM_ID, Token, TOKEN_PROGRAM_ID } from "@solana/spl-token";
import {
    CHAIN_ID_TERRA, CHAIN_ID_SOLANA,
    hexToUint8Array, nativeToHexString,
    transferFromSolana,
    setDefaultWasm,
    parseSequenceFromLogSolana,
    getEmitterAddressSolana,
    getSignedVAAWithRetry,
    redeemOnEth,
    redeemOnTerra,
} from "@certusone/wormhole-sdk";
import { BlockTxBroadcastResult, Coin, Coins, isTxError, LCDClient, MnemonicKey, MsgExecuteContract, MsgSend, MsgSwap, TxSuccess, Wallet as TerraWallet } from "@terra-money/terra.js";
import { SOLANA_HOST, SOLANA_DEVNET_CHAIN_ID, SOLANA_DEVNET_CORE_BRIDGE_ID, TERRA_SEED_PHRASE, TERRA_TESTNET_CHAIN_ID, TERRA_TESTNET_LCD_URL, TO_9_DECIMALS, UST_ON_SOLANA_ID, SOLANA_DEVNET_TOKEN_BRIDGE_ID, TERRA_CHAIN_IDS, WORMHOLE_TESTNET_RPC_URL, WORMHOLE_CHAIN_IDS, TERRA_TESTNET_TOKEN_BRIDGE_ID, TERRA_TESTNET_GAS_PRICES_URL, } from "./consts";


setDefaultWasm("node");
async function main() {
    try {
        // create a signer for Eth
        // const provider = new ethers.providers.WebSocketProvider(ETH_NODE_URL);
        // const signer = new ethers.Wallet(ETH_PRIVATE_KEY, provider);
        // const targetAddress = await signer.getAddress();


        // create a keypair for Solana
        // const keypair = Keypair.fromSecretKey(SOLANA_PRIVATE_KEY);
        // const payerAddress = keypair.publicKey.toString();
        const solUser = await SolanaUser.fromKeyfile();
        // console.log({ solUser: solUser.id() })

        const terraUser = await TerraUser.loadOrGen();
        // console.log({ terraUser: terraUser.accountAddress() })


        // await depositToBridgeOnTerra(terraUser, solUser);

        const depositInfo = await depositToBridgeOnSolana(solUser, terraUser);
        const signedVAA = await pollVaaConfirmation(depositInfo);
        const redeemInfo = await redeemFromBridgeOnTerra(terraUser, signedVAA);



        // provider.destroy(); // ethers provider
    } catch (e) {
        console.error(e);
    }
};


async function depositToBridgeOnTerra(terraUser: TerraUser, solUser: SolanaUser) {
    // assume terraUser has some LUNA
    // get accountInfo / balance
    const lunaBal = await terraUser.lunaBalance();
    console.log({ lunaBal });
    // TODO swap LUNA for UST
    // await swapToUstOnTerra(terraUser)
    await sendOnTerra(terraUser)
}

async function sendOnTerra(terraUser: TerraUser) {
    const send = new MsgSend(
        terraUser.wallet.key.accAddress,
        TerraUser.random().accountAddress(),
        { uluna: 1000 }
    );
    const tx = await terraUser.wallet.createAndSignTx({
        msgs: [send],
        memo: "Hello"
    });
    let txResult: BlockTxBroadcastResult = await terraUser.tx.broadcast(tx);
    if (isTxError(txResult)) {
        // console.log({ txResult })
        const { code, logs } = txResult;
        console.log({ code, logs })
        throw new Error(`encountered an error while running the transaction: ${txResult.code} ${txResult.codespace}`);
    }
    let { logs }: TxSuccess = txResult;
    console.log({ logs })
}



async function swapToUstOnTerra(terraUser: TerraUser) {
    // Create a message
    const msg = new MsgSwap(
        terraUser.accountAddress(),
        new Coin('uluna', '100000'),
        'uusd'
    );

    // Create and sign transaction
    const tx = await terraUser.wallet.createAndSignTx({
        msgs: [msg],
        memo: "test 1"
    });

    // Broadcast transaction
    const txResult = await terraUser.tx.broadcast(tx);

    // Get transaction hash
    const txHash = await terraUser.tx.hash(tx);
    console.log('txHash: ', txHash);

    if (isTxError(txResult)) {
        console.log({ txResult })
        throw new Error(`encountered an error while running the transaction: ${txResult.code} ${txResult.codespace}`);
    }

    // Check for events from the first message
    console.log('logs: ', txResult.logs);
}



export class TerraUser {
    wallet: TerraWallet;
    static ULUNA_TO_LUNA = 1_000_000;

    constructor(wallet: TerraWallet) {
        this.wallet = wallet;
    }
    public static async loadOrGen(): Promise<TerraUser> {
        const gasPrices = await (await fetch(TERRA_TESTNET_GAS_PRICES_URL)).json();
        const gasPricesCoins = new Coins(gasPrices);
        const lcd = new LCDClient({
            URL: TERRA_TESTNET_LCD_URL,
            chainID: TERRA_CHAIN_IDS.bombay,
            gasPrices: gasPricesCoins,
            gasAdjustment: "3",
            // gas: 10000000,
        });
        const mk = new MnemonicKey({
            mnemonic: TERRA_SEED_PHRASE ?? undefined,
        });
        if (!TERRA_SEED_PHRASE) {
            console.log({ seedPhrase: mk.mnemonic })
        }
        const wallet = lcd.wallet(mk);
        return new TerraUser(wallet)
    }
    public static random() {
        const lcd = new LCDClient({
            URL: TERRA_TESTNET_LCD_URL,
            chainID: TERRA_CHAIN_IDS.bombay,
        });
        const mk = new MnemonicKey();
        const wallet = lcd.wallet(mk);
        return new TerraUser(wallet)
    }
    public static async gasPrices(): Promise<Coins> {
        const gasPrices = await (await fetch(TERRA_TESTNET_GAS_PRICES_URL)).json();
        return new Coins(gasPrices);
    }

    public id(): string {
        return this.wallet.key.publicKey.address()
    }
    public accountAddress() {
        return this.wallet.key.accAddress
    }
    public get tx() {
        return this.wallet.lcd.tx
    }
    public lcd() {
        return this.wallet.lcd
    }
    public async lunaBalance(): Promise<number> {
        const [coins, _] = await this.wallet.lcd.bank.balance(this.wallet.key.accAddress);
        const bal = coins.get("uluna").amount;
        return bal.toNumber()
    }
}

const KEYPAIR_AUTHORITY_PATH = process.env.KEY_SOLANA_DEVNET;
export class SolanaUser {
    keys: Keypair;
    constructor(keys: Keypair) {
        this.keys = keys;
    }
    public static async fromKeyfile(): Promise<SolanaUser> {
        const fileText = await fs.readFile(KEYPAIR_AUTHORITY_PATH, "utf8");
        const privKeyBytes: Uint8Array = Uint8Array.from(JSON.parse(fileText).slice(0, 32)); // privKey is only first 32 bytes
        const keypair = Keypair.fromSeed(privKeyBytes);
        // const options = Provider.defaultOptions();
        // const ANCHOR_PROVIDER_URL = process.env.ANCHOR_PROVIDER_URL;
        // const connection = new Connection(ANCHOR_PROVIDER_URL, options.commitment);
        // @ts-ignore
        // let wallet = new anchor.Wallet(keypair);
        // let userProvider = new anchor.Provider(connection, wallet, options);
        return new SolanaUser(keypair);
    }
    public key(): PublicKey {
        return this.keys.publicKey
    }
    public id(): string {
        return this.key().toString()
    }
}

async function askTerraFaucet(terraUser: TerraUser) {
    // faucet to terra1vamrfqz2p55azcck90n6tnhma3m3gzz4xezqly
    try {
        const resp = await axios
            .post('https://faucet.terra.dev/claim', {
                address: terraUser.accountAddress(),
                chain_id: TERRA_CHAIN_IDS.bombay,
                lcd_url: TERRA_TESTNET_LCD_URL,
                denom: "uluna",
                response: "FDJJKFHJKSDFBHJLBJSHDKFBHK", // TODO captcha response ???
            });
    } catch (e) {
        console.error(`faucet axios err: ${e}`);
    }

    throw new Error("Function not implemented.");
}


async function depositToBridgeOnSolana(solUser: SolanaUser, terraUser: TerraUser): Promise<TransactionResponse> {
    // find the associated token account
    const fromAddress = (
        await Token.getAssociatedTokenAddress(
            ASSOCIATED_TOKEN_PROGRAM_ID,
            TOKEN_PROGRAM_ID,
            new PublicKey(UST_ON_SOLANA_ID),
            solUser.key()
        )
    ).toString();

    // // transfer the test token
    const connection = new Connection(SOLANA_HOST, "confirmed");
    // const amount = parseUnits("1", 9).toBigInt();
    const amount = BigInt(1 * TO_9_DECIMALS);
    const targetAddr = nativeToHexString(terraUser.id(), CHAIN_ID_TERRA);

    const transaction = await transferFromSolana(
        connection,
        SOLANA_DEVNET_CORE_BRIDGE_ID,
        SOLANA_DEVNET_TOKEN_BRIDGE_ID,
        solUser.id(),
        fromAddress,
        UST_ON_SOLANA_ID,
        amount,
        hexToUint8Array(targetAddr),
        CHAIN_ID_TERRA
    );

    // // sign, send, and confirm transaction
    transaction.partialSign(solUser.keys);


    const txid = await connection.sendRawTransaction(
        transaction.serialize()
    );
    await connection.confirmTransaction(txid);
    const depositInfo = await connection.getTransaction(txid);
    if (!depositInfo) {
        throw new Error(
            "An error occurred while fetching the transaction info"
        );
    }

    return depositInfo
}

async function accountTxs(terraUser: TerraUser) {
    // try {
    //     const { data: resp } = await axios.get(`https://bombay-fcd.terra.dev/v1/txs?offset=0&limit=100&account=${terraUser.accountAddress()}`);
    //     console.log({ resp });
    // } catch (e) {
    //     console.error(e);
    // }
}

async function redeemFromBridgeOnTerra(terraUser: TerraUser, signedVAA: any): Promise<BlockTxBroadcastResult> {
    const msg = await redeemOnTerra(
        TERRA_TESTNET_TOKEN_BRIDGE_ID, // TERRA_TOKEN_BRIDGE_ADDRESS,
        terraUser.wallet.key.accAddress,
        signedVAA
    );
    const gasPrices = await axios
        // .get(TERRA_GAS_PRICES_URL)
        .get(TERRA_TESTNET_GAS_PRICES_URL)
        .then((result) => result.data);
    const feeEstimate = await terraUser.tx.estimateFee(
        [
            {
                sequenceNumber: await terraUser.wallet.sequence(),
                publicKey: terraUser.wallet.key.publicKey,
            },
        ],
        {
            msgs: [msg],
            memo: "localhost",
            feeDenoms: ["uluna"],
            gasPrices,
        }
    );
    const tx = await terraUser.wallet.createAndSignTx({
        msgs: [msg],
        memo: "localhost",
        feeDenoms: ["uluna"],
        gasPrices,
        fee: feeEstimate,
    });
    const txRes = await terraUser.tx.broadcast(tx);

    // // expect(
    // //     await getIsTransferCompletedTerra(
    // //         TERRA_TOKEN_BRIDGE_ADDRESS,
    // //         signedVAA,
    // //         lcd,
    // //         TERRA_GAS_PRICES_URL
    // //     )
    // // ).toBe(true);
    // // provider.destroy();
    return txRes
}

type SignedVAA = Uint8Array;
async function pollVaaConfirmation(depositInfo: TransactionResponse): Promise<SignedVAA> {
    // get the sequence from the logs(needed to fetch the vaa)
    const sequence = parseSequenceFromLogSolana(depositInfo);
    const emitterAddress = await getEmitterAddressSolana(
        SOLANA_DEVNET_TOKEN_BRIDGE_ID, // SOLANA_TOKEN_BRIDGE_ADDRESS
    );
    // poll until the guardian(s) witness and sign the vaa
    const { vaaBytes: signedVAA } = await getSignedVAAWithRetry(
        [WORMHOLE_TESTNET_RPC_URL], // WORMHOLE_RPC_HOSTS,
        CHAIN_ID_SOLANA,
        emitterAddress,
        sequence,
        {
            transport: NodeHttpTransport(),
        }
    );
    return signedVAA
}


async function redeemFromBridgeOnEth() {
    // expect(
    //     await getIsTransferCompletedEth(
    //         ETH_TOKEN_BRIDGE_ADDRESS,
    //         provider,
    //         signedVAA
    //     )
    // ).toBe(false);
    // await redeemOnEth(ETH_TOKEN_BRIDGE_ADDRESS, signer, signedVAA);
    // expect(
    //     await getIsTransferCompletedEth(
    //         ETH_TOKEN_BRIDGE_ADDRESS,
    //         provider,
    //         signedVAA
    //     )
    // ).toBe(true);
}


main();



